import * as esbuild from 'esbuild';
import fs from 'node:fs';

await esbuild.build({
  entryPoints: ['./src/index.ts'],
  bundle: true,
  outdir: 'dist',
  platform: 'node',
  format: 'esm',
});

await fs.cpSync('./src/index.d.ts', './dist/index.d.ts');
