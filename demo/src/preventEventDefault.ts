function preventEventDefault(fn: () => unknown) {
  return (event: Event) => {
    event.preventDefault();

    return fn();
  }
}

export default preventEventDefault;
