function log(...args) {
  console.log(`[${new Date().toISOString()}]`, ...args);
}

function logErr(...args) {
  console.error(`[${new Date().toISOString()}]`, ...args);
}

export { log, logErr };
export default log;
