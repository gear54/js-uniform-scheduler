import { exec } from 'node:child_process';
import util from 'node:util';

const execPromise = util.promisify(exec);
const getLines = (string) => (string || '').split('\n');

function getExec({ log = console.log, error = console.error } = {}) {
  async function $(command, { logStdOut = true, logStdErr = true } = {}) {
    const { stdout: stdOut, stderr: stdErr } = await execPromise(command);
  
    if (stdOut && logStdOut) {
      log('stdout:', stdOut);
    }
  
    if (stdErr && logStdErr) {
      error('stderr:', stdErr);
    }
  
    return { stdOut: getLines(stdOut), stdErr: getLines(stdErr) };
  }

  return $;
}

export { getExec };
export default getExec();
