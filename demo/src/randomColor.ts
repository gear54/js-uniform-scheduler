const randomColor = () => {
  const number = (Math.random() * 0xfffff * 1000000).toString(16);

  return '#' + number.slice(0, 6);
}

export default randomColor;
