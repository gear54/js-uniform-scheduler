/** A task in the form of a function */
export type Task = (...args: unknown[]) => unknown;

/**
 * Schedules tasks uniformly over time given a minimum interval. The interval
 * denotes the minimum amount of time that has to elapse before a task can
 * be run after it was run previously.
 */
export class UniformScheduler {
  /**
   * @param minInterval Minimal amount of milliseconds that has to elapse before a given task can be run once again.
   */
  constructor(minInterval: number);

  /** Scheduler ensures that no task is run more often than this amount of milliseconds. */
  readonly minInterval: number;

  /** Number of tasks currently being scheduled. */
  get taskCount(): number;

  /**
   * Adds the task to the front of the line to be run. Assumes that newly added task can be run immediately.
   *
   * @param task A task to add to the line.
   */
  addTask(task: Task): void;

  /**
   * Removes the task from the line adjusting scheduling timings accordingly.
   *
   * @param task A task to remove from the line.
   */
  removeTask(task: Task): void;

  /** Removes all tasks and stops all timers. */
  stop(): void
}
