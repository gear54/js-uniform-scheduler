import { describe, test, expect } from '@jest/globals';

import getTasksMock from '~test/tasksMock';
import sleep from '~test/sleep';

import { UniformScheduler } from './UniformScheduler';

jest.spyOn(global, 'setTimeout');

let scheduler: UniformScheduler;
let tasksMock: ReturnType<typeof getTasksMock>;

describe('UniformScheduler', () => {
  beforeEach(() => {
    /**
     * We want enough ms to be unaffected by other event loop tasks
     * and also a number that is divisible evenly by many other numbers
     * to not be affected by rounding.
     */
    scheduler = new UniformScheduler(360);

    tasksMock = getTasksMock();
  });

  afterEach(() => {
    scheduler.stop();
    jest.clearAllMocks();
  });

  test('Should init without error', () => {
    scheduler = new UniformScheduler(1337);

    expect(scheduler).toBeInstanceOf(UniformScheduler);
    expect(scheduler.minInterval).toEqual(1337);
    expect(scheduler.taskCount).toEqual(0);
  });

  test('Should schedule one task immediately then stop', async () => {
    scheduler.addTask(tasksMock.getTask('a'));

    expect(setTimeout).toBeCalledWith(expect.any(Function), 0);
    expect(setTimeout).toBeCalledTimes(1);

    const countAdded = scheduler.taskCount;

    /** This includes 10ms leeway for any event loop uncertainties */
    await sleep(740);

    scheduler.stop();

    expect(countAdded).toEqual(1);
    expect(scheduler.taskCount).toEqual(0);

    expect(tasksMock.log).toEqualTaskLog([
      [0, 'task:a'],
      [360, 'task:a'],
      [360, 'task:a'],
    ]);
  });

  test('Should handle duplicate tasks', async () => {
    const taskA = tasksMock.getTask('a');

    scheduler.addTask(taskA);

    const countAddedFirst = scheduler.taskCount;

    await sleep(300);

    expect(() => {
      scheduler.addTask(taskA);
    }).toThrow('Failed to add task because it already exists in the line');

    const countAddedSecond = scheduler.taskCount;

    await sleep(440);

    scheduler.stop();

    expect(countAddedFirst).toEqual(1);
    expect(countAddedSecond).toEqual(1);

    expect(tasksMock.log).toEqualTaskLog([
      [0, 'task:a'],
      [360, 'task:a'],
      [360, 'task:a'],
    ]);
  });

  test('Should stop automatically when no more tasks are left', async () => {
    const taskA = tasksMock.getTask('a');
    const taskB = tasksMock.getTask('b');

    scheduler.addTask(taskB);
    scheduler.addTask(taskA);

    const countAdded = scheduler.taskCount;

    await sleep(190);

    scheduler.removeTask(taskA);
    scheduler.removeTask(taskB);

    await sleep(190);

    expect(countAdded).toEqual(2);
    expect(scheduler.taskCount).toEqual(0);

    expect(tasksMock.log).toEqualTaskLog([
      [0, 'task:a'],
      [180, 'task:b']
    ]);
  });

  test('Should not throw an error when removing a non-existing task', async () => {
    const taskA = tasksMock.getTask('a');

    scheduler.addTask(taskA);

    const countAdded = scheduler.taskCount;

    await sleep(370);

    scheduler.removeTask(taskA);
    scheduler.removeTask(() => {});

    await sleep(370);

    expect(countAdded).toEqual(1);
    expect(scheduler.taskCount).toEqual(0);

    expect(tasksMock.log).toEqualTaskLog([
      [0, 'task:a'],
      [360, 'task:a'],
    ]);
  });

  test('Should add two tasks and postpone the second', async () => {
    scheduler.addTask(tasksMock.getTask('a'));

    await sleep(90);

    /**
     * We wait only 90ms before adding the 2nd task but it should be
     * run 360/2=180ms after the first to preserve uniformity and minimum interval
     */
    scheduler.addTask(tasksMock.getTask('b'));

    const countAdded = scheduler.taskCount;

    await sleep(480);

    scheduler.stop();

    expect(countAdded).toEqual(2);
    expect(scheduler.taskCount).toEqual(0);

    expect(tasksMock.log).toEqualTaskLog([
      [0, 'task:a'],
      [180, 'task:b'],
      [180, 'task:a'],
      [180, 'task:b'],
    ]);
  });

  test('Should add two tasks and not postpone the second', async () => {
    scheduler.addTask(tasksMock.getTask('a'));

    await sleep(250);

    /**
     * We add the task later than 360/2=180ms after the first which means it can be run immediately
     * and the next task needs to be moved forward in time to preserve uniformity and minimum interval
     */
    scheduler.addTask(tasksMock.getTask('b'));

    const countAdded = scheduler.taskCount;

    await sleep(380);

    scheduler.stop();

    expect(countAdded).toEqual(2);
    expect(scheduler.taskCount).toEqual(0);

    expect(tasksMock.log).toEqualTaskLog([
      [0, 'task:a'],
      [250, 'task:b'],
      [180, 'task:a'],
      [180, 'task:b'],
    ]);
  });

  test('Should remove task that just ran and adjust timers', async () => {
    const taskA = tasksMock.getTask('a');
    const taskB = tasksMock.getTask('b');
    const taskC = tasksMock.getTask('c');

    scheduler.addTask(taskC);
    scheduler.addTask(taskB);
    scheduler.addTask(taskA);

    await sleep(300);

    // Task C just ran, removing it
    scheduler.removeTask(taskC);

    await sleep(530);

    scheduler.stop();

    expect(tasksMock.log).toEqualTaskLog([
      [0, 'task:a'],
      [120, 'task:b'],
      [120, 'task:c'],
      [120, 'task:a'],
      [180, 'task:b'],
      [180, 'task:a'],
    ]);
  });

  test('Should remove task that is next in line and adjust timers', async () => {
    const taskA = tasksMock.getTask('a');
    const taskB = tasksMock.getTask('b');
    const taskC = tasksMock.getTask('c');

    scheduler.addTask(taskC);
    scheduler.addTask(taskB);
    scheduler.addTask(taskA);

    // Task C just ran
    await sleep(300);

    // Removing the next in line
    scheduler.removeTask(taskA);

    await sleep(590);

    scheduler.stop();

    expect(tasksMock.log).toEqualTaskLog([
      [0, 'task:a'],
      [120, 'task:b'],
      [120, 'task:c'],
      [240, 'task:b'],
      [180, 'task:c'],
      [180, 'task:b'],
    ]);
  });

  test('Should remove task that is not next in line and adjust timers', async () => {
    const taskA = tasksMock.getTask('a');
    const taskB = tasksMock.getTask('b');
    const taskC = tasksMock.getTask('c');
    const taskD = tasksMock.getTask('d');

    scheduler.addTask(taskD);
    scheduler.addTask(taskC);
    scheduler.addTask(taskB);
    scheduler.addTask(taskA);

    await sleep(300);

    // Task D just ran, removing the 2nd in line
    scheduler.removeTask(taskB);

    await sleep(530);

    scheduler.stop();

    expect(tasksMock.log).toEqualTaskLog([
      [0, 'task:a'],
      [90, 'task:b'],
      [90, 'task:c'],
      [90, 'task:d'],
      [150, 'task:a'],
      [120, 'task:c'],
      [120, 'task:d'],
      [120, 'task:a'],
    ]);
  });

  test('Should remove two tasks at a time (higher index first) and adjust timers', async () => {
    const taskA = tasksMock.getTask('a');
    const taskB = tasksMock.getTask('b');
    const taskC = tasksMock.getTask('c');
    const taskD = tasksMock.getTask('d');
    const taskE = tasksMock.getTask('e');

    scheduler.addTask(taskE);
    scheduler.addTask(taskD);
    scheduler.addTask(taskC);
    scheduler.addTask(taskB);
    scheduler.addTask(taskA);

    await sleep(328);

    scheduler.removeTask(taskC);
    scheduler.removeTask(taskA);

    await sleep(568);

    scheduler.stop();

    expect(tasksMock.log).toEqualTaskLog([
      [0, 'task:a'],
      [72, 'task:b'],
      [72, 'task:c'],
      [72, 'task:d'],
      [72, 'task:e'],
      [168, 'task:b'],
      [120, 'task:d'],
      [120, 'task:e'],
      [120, 'task:b'],
    ]);
  });

  test('Should remove two adjacent tasks (lower index first) at a time and adjust timers', async () => {
    const taskA = tasksMock.getTask('a');
    const taskB = tasksMock.getTask('b');
    const taskC = tasksMock.getTask('c');
    const taskD = tasksMock.getTask('d');
    const taskE = tasksMock.getTask('e');

    scheduler.addTask(taskE);
    scheduler.addTask(taskD);
    scheduler.addTask(taskC);
    scheduler.addTask(taskB);
    scheduler.addTask(taskA);

    await sleep(350);

    scheduler.removeTask(taskA);
    scheduler.removeTask(taskB);

    await sleep(594);

    scheduler.stop();

    expect(tasksMock.log).toEqualTaskLog([
      [0, 'task:a'],
      [72, 'task:b'],
      [72, 'task:c'],
      [72, 'task:d'],
      [72, 'task:e'],
      [216, 'task:c'],
      [120, 'task:d'],
      [120, 'task:e'],
      [120, 'task:c'],
    ]);
  });

  test('Should add and remove task without running and adjust timings', async () => {
    const taskA = tasksMock.getTask('a');
    const taskB = tasksMock.getTask('b');
    const taskC = tasksMock.getTask('c');
    const taskD = tasksMock.getTask('d');

    scheduler.addTask(taskC);
    scheduler.addTask(taskB);
    scheduler.addTask(taskA);

    await sleep(260);

    scheduler.addTask(taskD);

    await sleep(10);

    scheduler.removeTask(taskD);

    await sleep(380);

    scheduler.stop();

    expect(tasksMock.log).toEqualTaskLog([
      [0, 'task:a'],
      [120, 'task:b'],
      [120, 'task:c'],
      [120, 'task:a'],
      [120, 'task:b'],
      [120, 'task:c'],
    ]);
  });

  test('Should remove task that never ran and adjust timings', async () => {
    const taskA = tasksMock.getTask('a');
    const taskB = tasksMock.getTask('b');
    const taskC = tasksMock.getTask('c');

    scheduler.addTask(taskC);
    scheduler.addTask(taskB);
    scheduler.addTask(taskA);

    // Task A just ran
    await sleep(30);

    scheduler.removeTask(taskB);

    await sleep(350);

    scheduler.stop();

    expect(tasksMock.log).toEqualTaskLog([
      [0, 'task:a'],
      [180, 'task:c'],
      [180, 'task:a'],
    ]);
  });
});
