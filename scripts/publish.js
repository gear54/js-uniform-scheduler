import { log, logErr } from './util/logger.js';
import GitLabApiClient from './util/gitlabApiClient.js';
import { getExec } from './util/exec.js';

const apiV4Url = process.env['CI_API_V4_URL'];
const refName = process.env['CI_COMMIT_REF_NAME'];
const commitHash = process.env['CI_COMMIT_SHA'];
const projectId = process.env['CI_PROJECT_ID'];
const projectPath = process.env['CI_PROJECT_PATH'];
const pipelineCommitAuthor = process.env['PIPELINE_COMMIT_AUTHOR'];
const pipelineId = parseInt(process.env['CI_PIPELINE_ID'], 10);
const jobId = parseInt(process.env['CI_JOB_ID'], 10);

// Has to be defined in project settings
const gitlabToken = process.env['REPO_ACCESS_TOKEN'];
const npmToken = process.env['NPM_ACCESS_TOKEN'];

/** From .gitlab-ci.yml */
const publishJobNames = ['publish-major', 'publish-minor', 'publish-patch'];

/** These statuses indicate that we can't run a publish job for the same SHA */
const lockedJobStatus = ['running', 'pending', 'scheduled', 'success', 'created'];

/** Passed to `npm version` */
const verisonType = process.argv[2];

const $ = getExec({ log, error: logErr });
const api = new GitLabApiClient(gitlabToken, projectId, apiV4Url);

if (refName !== 'master') {
  throw new Error(`Failed to publish from '${refName}', can only publish from 'master'`);
}

log(`Checking pipelines for commit '${commitHash}'`);

const pipelines = await api.getPipelinesForCommit(commitHash);

let pagesJobId = undefined;
let releaseJobId = undefined;

for (const pipeline of pipelines) {
  log(`Checking publish jobs in pipeline ${pipeline.web_url}`);

  const jobs = await api.getJobsForPipeline(pipeline.id);

  const publishJob = jobs.find((job) => {
    if (pipeline.id === pipelineId) {
      if (job.name === 'pages') {
        pagesJobId = job.id;
      }

      if (job.name === 'release') {
        releaseJobId = job.id;
      }
    }

    if (
      publishJobNames.includes(job.name)
      && lockedJobStatus.includes(job.status)
      && job.id !== jobId
    ) {
      return job;
    }
  });

  if (publishJob) {
    throw new Error(
      `Commit '${commitHash}' is already published or being published `
      + `by ${publishJob.web_url} (status: '${publishJob.status}')`
    );
  }
}

const [ _, gitName, gitEmail ] = /(.*) <(.*)>/.exec(pipelineCommitAuthor);

await $(`npm config set //registry.npmjs.org/:_authToken '${npmToken}'`);
await $(`git config --global user.name '${gitName}'`);
await $(`git config --global user.email '${gitEmail}'`);
await $(`git remote set-url origin https://user:${gitlabToken}@gitlab.com/${projectPath}.git/`);

await $('git checkout master --');

const { stdOut: [ headHash ] } = await $('git rev-parse HEAD', { logStdOut: false, logStdErr: false });

if (headHash !== commitHash) {
  throw new Error(`Pipeline running for '${commitHash}' but master is on '${headHash}'. Aborted!`);
}

await $(`npm version '${verisonType}' --message 'Publish ${commitHash} as v%s' > ./version`);
await $('git push --follow-tags');
await $('npm publish');

const triggerPromises = [];

if (pagesJobId) {
  log(`Triggering pages job #${pagesJobId}`);
  triggerPromises.push(api.triggerJob(pagesJobId));
} else {
  log('Not triggering pages job because its ID couldn\'t be determined');
}


if (releaseJobId) {
  log(`Triggering release job #${releaseJobId}`);
  triggerPromises.push(api.triggerJob(releaseJobId));
} else {
  log('Not triggering release job because its ID couldn\'t be determined');
}

await Promise.all(triggerPromises);
