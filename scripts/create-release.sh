#!/bin/sh

release-cli --server-url 'https://gitlab.com' \
  --private-token "$REPO_ACCESS_TOKEN" \
  --project-id "$CI_PROJECT_ID" \
  create \
  --tag-name "$1" \
  --name "Release $1" \
  --description 'Created by CI release job'
