import { expect } from '@jest/globals';

import type { MatcherFunction } from 'expect';

/** Delay (after the previous event), event specification */
type LogEntry = [number, string];

function getTasksMock() {
  let lastCall: number | undefined = undefined;

  const log: LogEntry[] = [];

  const getTask = (name: string) => {
    const namedFn = {
      [name]() {
        const now = Date.now();
  
        log.push([now - (lastCall ?? now), `task:${name}`]);
        lastCall = now;
      }
    } as const;

    // Having a function with dynamic name helps with debugging
    return namedFn[name] as () => void;
  };

  return {
    getTask,
    log,
  };
}

const toEqualTaskLog: MatcherFunction<[log: unknown, precision: unknown]> =
  function (actual, log, precision = 3) {
    if (!Array.isArray(actual)) {
      throw new TypeError('log must be an array');
    }

    if (!Array.isArray(log)) {
      throw new TypeError('log must be an array');
    }

    if (typeof precision !== 'number') {
      throw new TypeError('precision must be a number');
    }

    const mismatchMessage
      = `expected log:\n`
      + `${this.utils.printReceived(actual)}\nto match:\n`
      + `${this.utils.printExpected(log)}\n`
      + `with ${precision}ms precision`;

    if (actual.length !== log.length) {
      return {
        message: () => mismatchMessage,
        pass: false,
      };
    }

    const hasMismatch = actual.some(([delay, taskName], index) => {
      const [expectedDelay, expectedTaskName] = log[index];

      const min = expectedDelay - precision;
      const max = expectedDelay + precision;

      if (
        !(min <= delay && delay <= max)
        || taskName !== expectedTaskName
      ) {
        return true;
      }

      return false;
    });

    if (hasMismatch) {
      return {
        message: () => mismatchMessage,
        pass: false,
      };
    }

    return {
      message: () => 'expected the received log not to be the same as expected with specified precision',
      pass: true,
    };
  };

expect.extend({
  toEqualTaskLog,
});

declare module 'expect' {
  interface Matchers<R> {
    toEqualTaskLog(log: LogEntry[], precision?: number): R;
  }
}

export default getTasksMock;
