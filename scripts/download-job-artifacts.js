import { log, logErr } from './util/logger.js';
import GitLabApiClient from './util/gitlabApiClient.js';

// GitLab vars reference https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
const apiV4Url = process.env['CI_API_V4_URL'];
const projectId = process.env['CI_PROJECT_ID'];
const pipelineId = parseInt(process.env['CI_PIPELINE_ID'], 10);

// Has to be defined in project settings
const gitlabToken = process.env['REPO_ACCESS_TOKEN'];

const jobNames = process.argv.slice(2);

const api = new GitLabApiClient(gitlabToken, projectId, apiV4Url);

const jobs = await api.getJobsForPipeline(pipelineId);

log(`Downloading artifacts for jobs: '${jobNames.join('\', \'')}'`);

for (const name of jobNames) {
  const jobWithName = jobs.find(job => job.name === name);

  if (!jobWithName) {
    logErr(`Failed to find job '${name}'`);

    continue;
  }

  if (jobWithName.status !== 'success') {
    logErr(`Job '${name}' ${jobWithName.web_url} is not successful`);

    continue;
  }

  const fileName = `./artifacts-${name}.zip`;

  log(`Downloading artifacts for '${name}' ${jobWithName.web_url}`);
  await api.downloadJobArtifacts(jobWithName.id, `./artifacts-${name}.zip`);
  log(`Downloaded artifacts into '${fileName}'`);
}
