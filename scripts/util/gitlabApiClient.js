import fs from 'node:fs/promises';

class GitLabApiClient {
  #token;
  #projectId;
  #apiV4Url;

  constructor(token, projectId, apiV4Url = 'https://gitlab.com/api/v4') {
    this.#token = token;
    this.#projectId = projectId;
    this.#apiV4Url = apiV4Url;
  }

  get #headers() {
    return { 'PRIVATE-TOKEN': this.#token };
  }

  async getPipelinesForCommit(hash) {
    const url = `${this.#apiV4Url}/projects/${this.#projectId}/pipelines?sha=${hash}`;

    const response = await fetch(url, { headers: this.#headers });

    return response.json();
  }

  async getJobsForPipeline(id) {
    const url = `${this.#apiV4Url}/projects/${this.#projectId}/pipelines/${id}/jobs`;

    const response = await fetch(url, { headers: this.#headers });

    return response.json();
  }

  async triggerJob(id) {
    const url = `${this.#apiV4Url}/projects/${this.#projectId}/jobs/${id}/play`;

    const response = await fetch(
      url,

      {
        method: 'post',
        headers: this.#headers,
      },
    );

    return response.json();
  }

  async downloadJobArtifacts(id, path) {
    const url = `${this.#apiV4Url}/projects/${this.#projectId}/jobs/${id}/artifacts`;

    const response = await fetch(
      url,

      {
        method: 'get',
        headers: this.#headers,
      },
    );

    const buffer = await response.arrayBuffer();

    await fs.writeFile(path, Buffer.from(buffer));
  }
}

export default GitLabApiClient;
