import van from 'vanjs-core';
import { UniformScheduler, Task } from 'uniform-scheduler';

import randomColor from './randomColor';
import preventEventDefault from './preventEventDefault';

import './style.css';

const {
  div, input, form, label, article, fieldset, button,
  table, tbody, tr, td, section, img, a, h3,
} = van.tags;

const scheduler = van.state<UniformScheduler>();

const minInterval = van.state(1000);

// Needed because van.derive is suddenly async
const getDefaultTaskName = (index: number) => `task #${index}`;

const taskIndex = van.state(1);
const defaultTaskName = van.derive(() => getDefaultTaskName(taskIndex.val));
const taskName = van.state(defaultTaskName.val);
const taskColor = van.state(randomColor());

type TaskInfo = { id: string; name: string };

const tasks = van.state<(TaskInfo & { task: Task })[]>([]);

type TaskLog = (TaskInfo & { date: Date; color: string })[];

const taskLog = van.state<TaskLog>([]);

const taskLogFilters = van.state<TaskInfo[]>([]);
const selectedLogFilter = van.state<TaskInfo['id'] | undefined>();

const filteredTaskLog = van.derive(() =>
  taskLog.val.filter(({ id }) => {
    if (!selectedLogFilter.val) {
      return true;
    }

    return selectedLogFilter.val === id;
  })
);

const resetScheduler = () => {
  scheduler.val?.stop();
  scheduler.val = new UniformScheduler(minInterval.val);
  tasks.val = [];
  taskIndex.val = 1;
  taskName.val = getDefaultTaskName(taskIndex.val);
};

const addTask = () => {
  // We clear the task log on the first task instead of making a separate button
  if (scheduler.val.taskCount === 0) {
    taskLog.val = [];
    taskLogFilters.val = [];
  }

  const id = String(Date.now());
  const name = taskName.val ?? defaultTaskName.val;
  const color = taskColor.val;

  const task = () => {
    taskLog.val = [
      {
        id,
        date: new Date(),
        name,
        color,
      },

      ...taskLog.val,
    ];

    if (taskLog.val.length > 100) {
      taskLog.val.length = 100;
    }
  };

  tasks.val = [...tasks.val, { id, name, task }];
  taskLogFilters.val = [ { id, name }, ...taskLogFilters.val ];

  taskIndex.val = taskIndex.val + 1;
  taskName.val = getDefaultTaskName(taskIndex.val);
  taskColor.val = randomColor();

  scheduler.val.addTask(task);
};

const removeTask = (id: string) => {
  const taskRecord = tasks.val.find((task) => task.id === id);

  if (!taskRecord) {
    return;
  }

  scheduler.val.removeTask(taskRecord.task);
  tasks.val = tasks.val.filter((task) => task.id !== id);
};

const schedulerInterval = van.derive(() =>
  Math.floor(
    scheduler.val?.minInterval
    / Math.max(tasks.val.length, 1)
  )
);

const FilterButton = (filterValue: string | undefined, text: string) => button(
  {
    class: selectedLogFilter.val === filterValue ? 'secondary' : '',
    onclick: () => selectedLogFilter.val = filterValue,
  },

  text,
);

const Log = () => article(
  { class: 'log' },

  section(
    { class: 'grid' },

    () => `Task minimum interval: ${scheduler.val.minInterval}ms, scheduler interval: ${schedulerInterval.val}ms`,
  ),

  () => taskLogFilters.val.length ? section(
    { class: 'log-filter' },

    FilterButton(undefined, 'Show all'),

    taskLogFilters.val.map(({ id, name }) => FilterButton(id, `Only ${name}`)),
  ) : '',

  () =>
    table(
      tbody(
        filteredTaskLog.val.map(({ date, name, color }, index, log) => {
          const curDate = date.toISOString().split('T')[1].slice(0, -1);
          const prevDate = log.length > index + 1 ? log[index + 1].date : undefined;
          const prevDateStr = prevDate ? ` (+${date.getTime() - prevDate.getTime()}ms)` : '';

          return tr(
            td(`${curDate}${prevDateStr}`),
            td(name),
            td({ style: `background-color: ${color}` }),
          )
        }),
      )
    ),
);

const SchedulerForm = () =>
  article(
    form(
      { onsubmit: preventEventDefault(resetScheduler) },

      fieldset(
        { class: 'grid' },

        label(
          'Task minimum interval (ms)',

          input({
            type: 'number',

            value: minInterval,
            oninput: event => minInterval.val = parseInt(event.target.value, 10),
          }),
        ),
      ),

      input({ type: 'submit', value: 'Stop & reset scheduler' }),
    )
  );

const TaskForm = () => form(
  { onsubmit: preventEventDefault(addTask) },

  fieldset(
    div(
      { class: 'grid' },

      label(
        'Task name',

        input({
          type: 'text',
          placeholder: defaultTaskName,

          value: taskName,
          oninput: event => taskName.val = event.target.value,
        })
      ),

      label(
        'Task color',

        input({
          type: 'color',

          value: taskColor,
          oninput: event => taskColor.val = event.target.value,
        })
      ),
    ),
  ),

  input({ type: 'submit', value: 'Add task' }),
);

const TaskRemover = () => div(
  { class: 'task-remover' },

  tasks.val.map((task) =>
    button(
      {
        class: 'secondary',
        onclick: () => removeTask(task.id),
      },

      `Remove ${task.name}`,
    ),
  ),
);

const TaskControls = () => article(
  section(TaskForm()),
  () => tasks.val.length ? section(TaskRemover()) : '',
);

const headerText = 'libVersion' in window ? `Uniform Scheduler ${window.libVersion}` : 'Uniform Scheduler';

const Header = () => section(
  { class: 'flex header' },

  h3(headerText),

  div(
    img({ src: 'https://images.ctfassets.net/xz1dnu24egyd/3JZABhkTjUT76LCIclV7sH/17a92be9bce78c2adcc43e23aabb7ca1/gitlab-logo-500.svg' }),
    a({ href: 'https://gitlab.com/gear54/js-uniform-scheduler', target: '_blank' }, 'gitlab.com/gear54/js-uniform-scheduler'),
  ),
);

const App = () => [
  Header(),

  div(
    { class: 'grid main-columns' },

    Log(),

    div(
      { class: 'controls-column'},

      SchedulerForm(),
      TaskControls(),
    ),
  ),
];

resetScheduler();

// Mount
const appEl = document.querySelector('#app');

if (!appEl) {
  throw new Error('Failed to find #app');
}

van.add(appEl, App());
