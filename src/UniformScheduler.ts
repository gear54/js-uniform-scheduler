type Task = (...args: unknown[]) => unknown;

class UniformScheduler {
  /** First task in the array is next in the line to be executed  */
  private tasks: Task[] = [];

  private timeout: NodeJS.Timeout | undefined;

  private boundLoop: () => void;

  /** Unix(ms) time when the next task is going to be executed */
  private nextFire = 0;

  /** Unix(ms) time for each task */
  private lastFiredTasks = new WeakMap<Task, number>();

  constructor(public readonly minInterval: number) {
    this.boundLoop = () => this.loop();
  }

  private get lastFire(): number {
    const { tasks } = this;
    const lastTask = tasks[tasks.length - 1];

    // Shouldn't ever happen as we only access `lastFire` when we have tasks
    if (!lastTask) {
      throw new Error('Failed to determine last executed task');
    }

    return this.lastFiredTasks.get(lastTask) ?? 0;
  }

  get taskCount(): number {
    return this.tasks.length;
  }

  // The interval after which we can run the next task in the line
  private get interval(): number {
    return Math.ceil(this.minInterval / Math.max(1, this.tasks.length));
  }

  private loop(): void {
    const task = this.tasks.shift();

    // Shouldn't ever happen as we stop timers when the last task is removed
    if (!task) {
      throw new Error('Failed to determine next task to be executed');
    }

    const now = Date.now();

    task();

    this.tasks.push(task);
    this.lastFiredTasks.set(task, now);
    this.nextFire = now + this.interval;

    this.scheduleLoop();
  }

  /** [Re-]schedule the next task using the current `nextFire` value */
  private scheduleLoop(): void {
    clearTimeout(this.timeout)

    this.timeout = setTimeout(this.boundLoop, this.nextFire - Date.now());
  }

  addTask(task: Task): void {
    const { tasks } = this;

    if (tasks.includes(task)) {
      throw new Error('Failed to add task because it already exists in the line');
    }

    const now = Date.now();

    tasks.unshift(task);

    if (this.nextFire) {
      this.nextFire = Math.max(this.lastFire + this.interval, now);
    } else {
      this.nextFire = now;
    }

    this.scheduleLoop();
  }

  removeTask(task: Task): void {
    const { tasks, minInterval } = this;
    const index = tasks.indexOf(task);

    if (index === -1) {
      return;
    }

    /** Last task means we have to clear timers */
    if (tasks.length === 1) {
      this.stop();

      return;
    }

    tasks.splice(index, 1);

    /**
     * For each task we calculate in which amount of milliseconds
     * it will run if we don't make any timing adjustments. We then
     * take the smallest interval.
     */
    const smallestPotentialInterval = tasks.reduce((acc, task, index) => {
      const lastFire = this.lastFiredTasks.get(task) ?? 0;
      const nextFire = this.lastFire + this.interval * (index + 1);

      return Math.min(acc, nextFire - lastFire);
    }, minInterval);

    /**
     * The difference between `minInterval` and the aforementioned smallest potential
     * interval is the adjustment we need to apply to the normal `interval`.
     */
    this.nextFire = this.lastFire + minInterval - smallestPotentialInterval + this.interval;

    this.scheduleLoop();
  }

  stop(): void {
    clearTimeout(this.timeout);

    this.nextFire = 0;
    this.timeout = undefined;
    this.tasks = [];
  }
}

export {
  type Task,

  UniformScheduler,
};
